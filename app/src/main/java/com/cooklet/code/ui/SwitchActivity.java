package com.cooklet.code.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.view.View;

import com.cooklet.code.CameraActivity;
import com.cooklet.code.R;
import com.cooklet.code.databinding.ActivitySwitchBinding;
import com.cooklet.code.ui.model.BaseViewModel;

/**
 *@author Tomasz Trybala
 */
public class SwitchActivity extends BaseActivity{
    @Override
    protected void bind() {
        ActivitySwitchBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_switch);
        binding.setModel(new BaseViewModel(this));
    }

    public void onCamera(View v){
        Intent intent = new Intent(SwitchActivity.this, CameraActivity.class);
        startActivity(intent);
    }

    public void onProduct(View v){
        Intent intent = new Intent(SwitchActivity.this, ProductActivity.class);
        startActivity(intent);
    }
}
