package com.cooklet.code.ui.model;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.databinding.BaseObservable;
import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.TextView;

import com.cooklet.code.R;
import com.cooklet.code.fonts.RobotoBold;
import com.cooklet.code.fonts.RobotoLight;
import com.cooklet.code.fonts.RobotoMedium;
import com.cooklet.code.fonts.RobotoRegular;
import com.cooklet.code.ui.BaseActivity;

import java.lang.ref.WeakReference;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybala
 */
public class BaseViewModel extends BaseObservable {
    private final WeakReference<BaseActivity> mActivity;

    protected
    @NonNull
    Context getActivityContext() {
        return mActivity.get();
    }

    protected
    @NonNull
    Resources getResources() {
        return mActivity.get().getResources();
    }

    protected
    @NonNull
    FragmentManager getFragmentManager() {
        return mActivity.get().getSupportFragmentManager();
    }

    protected void finish() {
        mActivity.get().finish();
    }

    public BaseViewModel(@NonNull BaseActivity activity) {
        mActivity = new WeakReference<>(activity);
    }

    public void showSnackbarMessage(@StringRes int message, int duration, @NonNull View root) {
        checkNotNull(root);
        Snackbar.make(root, message, duration).show();
    }

    public void showSnackbarMessage(@StringRes int message, int duration, final @NonNull Runnable onFinishAction) {
        Activity activity = mActivity.get();
        //noinspection ConstantConditions
        if (activity != null) {
            View rootView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
            Snackbar s = Snackbar.make(rootView, message, duration);
            s.setCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(@NonNull Snackbar snackbar, int event) {
                    onFinishAction.run();
                }
            });
            s.show();
        }
    }

    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, String fontName) {
        switch (fontName) {
            case "medium":
                textView.setTypeface(getRobotoMedium());
                break;
            case "bold":
                textView.setTypeface(getRobotoBold());
                break;
            case "light":
                textView.setTypeface(getRobotoLight());
                break;
            default:
                textView.setTypeface(getRobotoRegular());
                break;
        }
    }

    private static Typeface getRobotoMedium() {
        return RobotoMedium.getTypeface();
    }

    private static Typeface getRobotoRegular() {
        return RobotoRegular.getTypeface();
    }

    private static Typeface getRobotoLight() {
        return RobotoLight.getTypeface();
    }

    private static Typeface getRobotoBold() {
        return RobotoBold.getTypeface();
    }
}
