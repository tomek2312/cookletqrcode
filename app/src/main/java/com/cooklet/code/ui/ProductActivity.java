package com.cooklet.code.ui;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;

import com.cooklet.code.BundleConstants;
import com.cooklet.code.R;
import com.cooklet.code.databinding.ActivityProductBinding;
import com.cooklet.code.model.Product;
import com.cooklet.code.utils.StringEncryptor;
import com.google.common.base.Optional;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import java.util.EnumMap;

/**
 * @author Tomasz Trybala
 */
public class ProductActivity extends BaseActivity {
    private ActivityProductBinding mBinding;
    private Product mProduct = new Product();
    private int mSize;

    private void measureImage() {
        mBinding.imgvQrCode.post(new Runnable() {
            @Override
            public void run() {
                mSize = Math.min(mBinding.imgvQrCode.getMeasuredWidth(),
                        mBinding.imgvQrCode.getMeasuredHeight());
            }
        });
    }

    public void onEncode(View v) {
        hideKeyboard();
        mProduct.setId(mBinding.edtId.getText().toString());
        mProduct.setUsed(mBinding.acbUsed.isChecked());
        mProduct.setName(mBinding.edtName.getText().toString());
        StringEncryptor encryptor = new StringEncryptor();
        Optional<String> optString = encryptor.encrypt(mProduct.toGson());
        if (optString.isPresent()) {
            BarcodeFormat barcodeFormat = BarcodeFormat.QR_CODE;
            int colorBack = 0xFF000000;
            int colorFront = 0xFFFFFFFF;

            QRCodeWriter writer = new QRCodeWriter();
            try {
                EnumMap<EncodeHintType, Object> hint = new EnumMap<>(EncodeHintType.class);
                hint.put(EncodeHintType.CHARACTER_SET, "UTF-8");
                BitMatrix bitMatrix = writer.encode(optString.get(), barcodeFormat, mSize, mSize, hint);
                int width = bitMatrix.getWidth();
                int height = bitMatrix.getHeight();
                int[] pixels = new int[width * height];
                for (int y = 0; y < height; y++) {
                    int offset = y * width;
                    for (int x = 0; x < width; x++) {

                        pixels[offset + x] = bitMatrix.get(x, y) ? colorBack : colorFront;
                    }
                }

                Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
                mBinding.imgvQrCode.setImageBitmap(bitmap);
            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
    }

    private void load() {
        Intent intent = getIntent();
        if (intent != null) {
            Bundle extras = intent.getExtras();
            if (extras != null && extras.containsKey(BundleConstants.EXTRA_PRODUCT)) {
                mProduct = (Product) extras.getSerializable(BundleConstants.EXTRA_PRODUCT);
            }
        }
    }

    @Override
    protected void bind() {
        load();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product);
        mBinding.setProduct(mProduct);
        measureImage();
    }
}
