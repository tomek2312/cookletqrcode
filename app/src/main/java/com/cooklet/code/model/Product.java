package com.cooklet.code.model;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @author Tomasz Trybala
 */
public class Product implements Serializable{
    @SerializedName("id")
    public String mId;

    @SerializedName("name")
    public String mName;

    @SerializedName("used")
    public boolean mUsed;

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public boolean isUsed() {
        return mUsed;
    }

    public void setUsed(boolean mUsed) {
        this.mUsed = mUsed;
    }

    public String toGson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
