package com.cooklet.code;

/**
 * @author Tomasz Trybala
 */
public class BundleConstants {
    public static final String SETTINGS_NAME = "appPrefs";
    public static final String EXTRA_PRODUCT = "product_extra";
}
