package com.cooklet.code.fonts;

import android.graphics.Typeface;

import com.cooklet.code.CookletApplication;

/**
 * Singleton class.
 * Returns Roboto Light Typeface.
 *
 *@author Tomasz Trybała
 */
public class RobotoLight {
    private static Typeface sTypeface;

    public static Typeface getTypeface(){
        if(sTypeface == null){
            sTypeface = Typeface.createFromAsset(CookletApplication.getAppContext().getAssets(),
                    "fonts/RobotoLight.ttf");
        }

        return sTypeface;
    }
}
