package com.cooklet.code.fonts;

import android.graphics.Typeface;

import com.cooklet.code.CookletApplication;

/**
 * Singleton class.
 * Returns Roboto Bold Typeface.
 *
 *@author Tomasz Trybała
 */
public class RobotoBold {
    private static Typeface sTypeface;

    public static Typeface getTypeface(){
        if(sTypeface == null){
            sTypeface = Typeface.createFromAsset(CookletApplication.getAppContext().getAssets(),
                    "fonts/RobotoBold.ttf");
        }

        return sTypeface;
    }
}
