package com.cooklet.code.fonts;

import android.graphics.Typeface;

import com.cooklet.code.CookletApplication;


/**
 * Singleton class.
 * Returns Roboto Medium Typeface.
 *
 *@author Tomasz Trybała
 */
public class RobotoMedium {
    private static Typeface sTypeface;

    public static Typeface getTypeface(){
        if(sTypeface == null){
            sTypeface = Typeface.createFromAsset(CookletApplication.getAppContext().getAssets(),
                    "fonts/RobotoMedium.ttf");
        }

        return sTypeface;
    }
}
