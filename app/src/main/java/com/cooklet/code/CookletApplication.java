
package com.cooklet.code;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDexApplication;
import android.view.Display;
import android.view.WindowManager;

import com.cooklet.code.utils.StringEncryptor;
import com.google.common.base.Optional;
import com.google.common.eventbus.EventBus;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Tomasz Trybała
 * @since 03.03.2015
 */
public class CookletApplication extends MultiDexApplication {
    //region variables
    private static EventBus sEventBus;
    private static Context sContext;
    private static CookletApplication sInstance;
    //endregion

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        sContext = getApplicationContext();
        sEventBus = new EventBus("cooklet search event bus");
    }

    /**
     * Method returns synchronized instance of application.
     *
     * @return application instance
     */
    public static synchronized CookletApplication getInstance() {
        return sInstance;
    }

    /**
     * Returns width of screen
     *
     * @param activity current activity
     * @return screen width
     */
    @SuppressWarnings("deprecation")
    @SuppressLint("NewApi")
    public static int getScreenWidth(@NonNull Activity activity) {
        int measuredWidth;
        WindowManager windowManager = activity.getWindowManager();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            windowManager.getDefaultDisplay().getSize(size);
            measuredWidth = size.x;
        } else {
            Display d = windowManager.getDefaultDisplay();
            measuredWidth = d.getWidth();
        }

        return measuredWidth;
    }

    /**
     * Saves encrypted string value in shared preferences.
     *
     * @param key   key in shared preferences
     * @param value value in shared preferences
     */
    public static void saveToSettings(@NonNull String key, @NonNull String value) {
        checkNotNull(key, "key cannot be null");
        checkNotNull(value, "value cannot be null");

        SharedPreferences settings = sContext.getSharedPreferences(
                BundleConstants.SETTINGS_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        StringEncryptor encryptor = new StringEncryptor();
        Optional<String> encryptedValue = encryptor.encrypt(value);

        if (encryptedValue.isPresent()) {
            String encrypted = encryptedValue.get();
            preferencesEditor.putString(key, encrypted);
        } else {
            preferencesEditor.putString(key, value);
        }

        preferencesEditor.apply();
    }

    /**
     * Saves encrypted integer value in shared preferences.
     *
     * @param key   key in shared preferences
     * @param value value in shared preferences
     */
    public static void saveToSettings(@NonNull String key, int value) {
        saveToSettings(key, "" + value);
    }

    /**
     * Saves encrypted boolean value in shared preferences.
     *
     * @param key   key in shared preferences
     * @param value value in shared preferences
     */
    public static void saveToSettings(@NonNull String key, boolean value) {
        saveToSettings(key, value ? Boolean.TRUE.toString() :
                Boolean.FALSE.toString());
    }

    /**
     * Load decrypted string value from shared preferences.
     *
     * @param key key in shared preferences
     * @return optional of String. Optional is present if key is in
     * shared preferences.
     */
    public static
    @NonNull
    Optional<String> loadFromSettings(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = sContext.getSharedPreferences(
                BundleConstants.SETTINGS_NAME, Activity.MODE_PRIVATE);
        String encryptedPassword = settings.getString(key, null);
        String decryptedValue = encryptedPassword;

        if (encryptedPassword != null) {
            StringEncryptor encryptor = new StringEncryptor();
            Optional<String> decryptedPassword = encryptor.decrypt(encryptedPassword);
            if (decryptedPassword.isPresent()) {
                decryptedValue = decryptedPassword.get();
            }
        }

        return Optional.fromNullable(decryptedValue);
    }

    /**
     * Load decrypted string value from shared prefernces.
     *
     * @param key key in shared preferences
     * @return optional of String. Optional is present if key is in
     * shared preferences.
     */
    public static
    @NonNull
    Optional<String> loadFromSettings(@NonNull String key, @NonNull Context context) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = context.getSharedPreferences(
                BundleConstants.SETTINGS_NAME, Activity.MODE_PRIVATE);
        String encryptedPassword = settings.getString(key, null);
        String decryptedValue = encryptedPassword;

        if (encryptedPassword != null) {
            StringEncryptor encryptor = new StringEncryptor();
            Optional<String> decryptedPassword = encryptor.decrypt(encryptedPassword);
            if (decryptedPassword.isPresent()) {
                decryptedValue = decryptedPassword.get();
            }
        }

        return Optional.fromNullable(decryptedValue);
    }

    /**
     * Method loads integer from shared preferences;
     *
     * @param key key in shared preferences
     * @return integer from shared preferences.
     * If no exists, returns -1;
     */
    public static int loadIntegerFromSettings(@NonNull String key) {
        Optional<String> optional = loadFromSettings(key);
        if (optional.isPresent()) {
            return Integer.parseInt(optional.get());
        } else {
            return -1;
        }
    }

    /**
     * Method loads integer from shared preferences;
     *
     * @param key     key in shared preferences
     * @param context current context
     * @return integer from shared preferences.
     * If no exists, returns -1;
     */
    public static int loadIntegerFromSettings(@NonNull String key, Context context) {
        Optional<String> optional = loadFromSettings(key, context);
        if (optional.isPresent()) {
            return Integer.parseInt(optional.get());
        } else {
            return -1;
        }
    }

    /**
     * Method loads String from shared preferences;
     *
     * @param key key in shared preferences
     * @return String from shared preferences.
     * If no exists, returns empty String;
     */
    public static
    @NonNull
    String loadStringFromSettings(@NonNull String key) {
        Optional<String> optional = loadFromSettings(key);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return "";
        }
    }

    /**
     * Method loads String from shared preferences;
     *
     * @param key     key in shared preferences
     * @param context current context
     * @return String from shared preferences.
     * If no exists, returns empty String;
     */
    public static
    @NonNull
    String loadStringFromSettings(@NonNull String key, Context context) {
        Optional<String> optional = loadFromSettings(key, context);
        if (optional.isPresent()) {
            return optional.get();
        } else {
            return "";
        }
    }

    /**
     * Method loads boolean from shared preferences;
     *
     * @param key key in shared preferences
     * @return boolean from shared preferences.
     * If no exists, returns false;
     */
    public static boolean loadBooleanFromSettings(@NonNull String key) {
        Optional<String> optional = loadFromSettings(key);
        return optional.isPresent() && optional.get().equals(Boolean.TRUE.toString());
    }

    /**
     * Method removes value in shared preferences by key.
     *
     * @param key key in shared preferences
     */
    public static void clearSetting(@NonNull String key) {
        checkNotNull(key, "key cannot be null");

        SharedPreferences settings = sContext.getSharedPreferences(BundleConstants.SETTINGS_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.remove(key);
        editor.apply();
    }

    /**
     * Method removes all values in shared preferences.
     */
    public static void clearAllSettings() {
        SharedPreferences settings = sContext.getSharedPreferences(BundleConstants.SETTINGS_NAME,
                Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
    }

    /**
     * @return EventBus object
     */
    public static
    @NonNull
    EventBus getEventBus() {
        return sEventBus;
    }

    /**
     * @return context of application
     */
    public static
    @NonNull
    Context getAppContext() {
        return sContext;
    }
}
