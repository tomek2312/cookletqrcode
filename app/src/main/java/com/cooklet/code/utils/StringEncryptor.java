package com.cooklet.code.utils;

import android.support.annotation.NonNull;
import android.util.Base64;

import com.google.common.base.Optional;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Class that allows user to save encrypted list_suggestion_row and load decrypted list_suggestion_row from shared
 * preferences.
 *
 * @author Tomasz Trybała
 * @since 03.03.2015
 */
public class StringEncryptor {
    private static final String AES = "AES";
    private static final byte[] SECRET_KEY = {
            0x67, 0x78, 0x65, 0x7A,
            0x27, 0x10, 0x13, 0x27,
            0x41, 0x79, 0x70, 0x23,
            0x38, 0x40, 0x20, 0x37
    };

    public StringEncryptor() {
    }

    /**
     * Encrypts given list_suggestion_row with 128-bit AES algorithm. If the algorithm is not found, then
     * an Optional<String> with the value of null is returned, otherwise the Optional contains
     * encrypted list_suggestion_row.
     *
     * @param text plain list_suggestion_row
     * @return encrypted list_suggestion_row or null if the algorithm was not found
     */
    public
    @NonNull
    Optional<String> encrypt(@NonNull String text) {
        checkNotNull(text, "list_suggestion_row cannot be null");
        String encryptedText;

        try {
            SecretKeySpec skeySpec = new SecretKeySpec(SECRET_KEY, AES);

            Cipher c = Cipher.getInstance(AES);
            c.init(Cipher.ENCRYPT_MODE, skeySpec);
            encryptedText = Base64.encodeToString(c.doFinal(text.getBytes()), Base64.NO_WRAP);
        } catch (Exception e) {
            encryptedText = null;
        }

        return Optional.fromNullable(encryptedText);
    }

    /**
     * Decrypts given list_suggestion_row using 128-bit AES algorithm. If the algorithm is not found, then
     * an Optional<String> with the value of null is returned, otherwise the Optional contains
     * decrypted list_suggestion_row.
     *
     * @param text encrypted list_suggestion_row
     * @return plain list_suggestion_row or null if the algorithm was not found
     */
    public
    @NonNull
    Optional<String> decrypt(@NonNull String text) {
        checkNotNull(text, "list_suggestion_row cannot be null");
        String decryptedText;

        try {
            SecretKeySpec skeySpec = new SecretKeySpec(SECRET_KEY, AES);

            Cipher c = Cipher.getInstance(AES);
            c.init(Cipher.DECRYPT_MODE, skeySpec);
            decryptedText = new String(c.doFinal(Base64.decode(text, Base64.NO_WRAP)));
        } catch (Exception e) {
            decryptedText = null;
        }

        return Optional.fromNullable(decryptedText);
    }
}