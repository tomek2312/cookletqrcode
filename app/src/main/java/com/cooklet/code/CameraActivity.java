package com.cooklet.code;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.cooklet.code.model.Product;
import com.cooklet.code.ui.ProductActivity;
import com.cooklet.code.utils.StringEncryptor;
import com.google.common.base.Optional;
import com.google.gson.Gson;

import me.dm7.barcodescanner.zbar.Result;
import me.dm7.barcodescanner.zbar.ZBarScannerView;

public class CameraActivity extends AppCompatActivity implements ZBarScannerView.ResultHandler {
    protected static final int REQUEST_CAMERA = 100;
    private ZBarScannerView mScannerView;

    protected boolean checkPermissionForCamera() {
        return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA);
    }

    protected void requestPermissionForCamera() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            View rootView = getWindow().getDecorView().findViewById(android.R.id.content);
            Snackbar.make(rootView, "rationaleExternalStorage",
                    Snackbar.LENGTH_LONG).show();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                            Manifest.permission.CAMERA},
                    REQUEST_CAMERA);
        }
    }

    private void startCamera() {
        if (!checkPermissionForCamera()) {
            requestPermissionForCamera();
        } else {
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
        }
    }

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZBarScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        Gson gson = new Gson();
        StringEncryptor encryptor = new StringEncryptor();
        Optional<String> optString = encryptor.decrypt(rawResult.getContents());

        if (optString.isPresent()) {
            Intent intent = new Intent(CameraActivity.this, ProductActivity.class);
            intent.putExtra(BundleConstants.EXTRA_PRODUCT, gson.fromJson(optString.get(), Product.class));
            startActivity(intent);
            finish();
        } else {
            Toast.makeText(CameraActivity.this, rawResult.getContents(), Toast.LENGTH_SHORT).show();
        }

        startCamera();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mScannerView.startCamera();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
